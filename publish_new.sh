#!/bin/sh

guix shell --container \
     --share=/home/frederick/Public \
     emacs emacs-ox-rss emacs-f emacs-htmlize \
     -- emacs -Q --script publish.el
