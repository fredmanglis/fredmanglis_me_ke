(use-modules (haunt site)
	     (haunt asset)
	     (haunt builder blog)
	     (haunt builder atom)
	     (haunt reader skribe)
	     (haunt builder assets)

	     (fredm themes blog)
	     (fredm themes static-page)
	     (fredm builder static-page))

(site #:title "Frederick's Aerie"
      #:domain "fredmanglis.me.ke"
      #:build-directory "web"
      #:default-metadata
      '((author . "Frederick M. Muriithi"))
      #:readers (list skribe-reader)
      #:builders
      (list (blog #:theme blog-theme)
	    (atom-feed)
	    (atom-feeds-by-tag)
	    (static-directory "static")
	    (static-directory "images")
	    (static-page "pages" static-page-theme)))
