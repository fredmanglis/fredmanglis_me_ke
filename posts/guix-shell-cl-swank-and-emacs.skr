(use-modules ((fredm utils) #:prefix fmu:))

(post
 :date (make-date* 2022 05 21)
 :tags '("guix" "emacs" "common-lisp" "swank")
 :title "Running a Swank Server Within a Guix Shell"

 (section
  (h1 [Steps])

  (p [First off, add the ,(strong [sbcl-slime-swank]) package to your manifest. In case you do not want to add it to the manifest that will be used for the production system, you could create a new ,(strong [dev-manifest.scm]) and "import" the production manifest.])

  (p [,(fmu:source-code
	[
	 ;; dev-manifest.scm
	 (use-modules (guix profiles))

	 (define %runtime-manifest (load "./prod-manifest.scm"))

	 (concatenate-manifests
	  (list %runtime-manifest
		(specifications->manifest
		 (list "sbcl-slime-swank"))))
	 ]
	:class "language-lisp")])

  (p [The next step is to declare that your project depends on ,(strong [swank]) within your project's ,(strong [.asd]) file])

  (p [,(fmu:source-code
	[
	 ;; the-awesome-project.asd
	 (asdf:defsystem #:awesome-project
			 :description "An awesome project"
			 :author "Me <my@ema.il>"
			 :license  "MIT"
			 :version "0.0.1"
			 :serial t
			 :pathname "src"
			 :depends-on (...
  				      ...
				      #:swank)
			 :components ((:file ...)))]
	:class "language-lisp")])

  (p [Now you can launch the shell:
	  ,(code
	    [guix shell --container --network --manifest=dev-manifest.scm])])

  (aside [,(p [The ,(em [--container]) option runs the shell within a container that is isolated from the rest of your system. This means that nothing leaks into your runtime environment unless you explicitly specify that it does. It is especially nice at catching dependencies that are not explicitly declared.])

	  ,(p [The ,(em [--network]) allows the container to use your network connection and to share ports with the host without the hassle of setting up ssh forwarding.])
	  ])

  (p [Within the shell in the container, you can now start the lisp image and launch the swank server.])

  (p [Launch your CL implementation and load the application definitions
	     ,(fmu:source-code
	       [
		$ sbcl
		  * (require 'asdf)
		  < some output ... >
		  * (asdf:load-asd (merge-pathnames "qc-uploads.asd" (uiop/os:getcwd)))
		  < some output ... >
		  * (asdf:load-system :qc-uploads)
		  < possibly a lot of output as files are compiled ... >
		  * (swank:create-server)
		  ;; Swank started at port: 4005.
		  4005]
	       :class "language-lisp")])

  (p [And now you have a swank server running within a guix shell container, that, thanks to the ,(em [--network]) option, can be accessed on you host system.])

  (p [On your emacs, you can connect to the swank server with ,(em [M-x slime-connect]), set your host as "localhost" (without the quotes) and the port to the port selected when starting the swank server (default 4005).])

  (p [Now go forth and embrace the parens!!!]))

 (section
  (h1 [Context and Motivation])

  (p [I found myself working on a ,(fmu:anchor "Common Lisp project" "https://git.genenetwork.org/jgart/qc-uploads" "GeneNetwork's QC Application") to do a few minor fixes.])

  (p [The first thing I tried was to run the thing within a guix shell, and immediately caught a number of issues, among which were the fact that you can't really run quicklisp within a guix shell container ,(em [guix shell --container]).])

  (p [This is my attempt to make this easier for the next person who might need it.]))

 (fmu:import-css "https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.6.0/styles/default.min.css")
 (fmu:highlight-js)
 (fmu:import-js "https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.6.0/languages/lisp.min.js")
 `(script "hljs.highlightAll()"))
