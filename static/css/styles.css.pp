#lang pollen

body {
    margin: 40px auto;
    max-width: 650px;
    line-height: 1.6;
    font-size: 18px;
    background: #EEEEEE;
    padding: 0 10px;
}

h1,h2,h3 {
    line-height: 1.2;
}

#prev-next-nav {
    display: grid;
    grid-column-gap: 50px;
}

.nav-entry{
    display: block;
    padding: 0 0.5em 0 0.5em;
}
.nav-entry:nth-of-type(2n){
    background: #DDDDDD;
}
