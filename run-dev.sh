#!/bin/sh

# Install required packages if necessary
yes | raco pkg install pollen
yes | raco pkg install gregor

# Remove emacs temporary files
find ./ -name '*~' -type f -print0 | xargs -0 -r rm -v

# Build the blog pagetree
rm blog.ptree
racket build-ptree.rkt

raco pollen start
