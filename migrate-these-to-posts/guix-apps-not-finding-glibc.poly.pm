#lang pollen

◊(define-meta publish-date "2020-07-18T17:51:00+03:00")
◊(define-meta categories '("GNU" "Guix" "Ubuntu 20.04"))

◊h1{Fix: Application Installed via GNU Guix Could Not "Find" GLibc}

◊h2{Introduction}

Recently, I have upgraded my Ubuntu installation from ◊strong{18.04 LTS} to ◊strong{20.04 LTS}.

Upgrading Ubuntu failed somewhere, and I had to troubleshoot and fix by manually uninstalling some really low-level packages and reinstalling newer ones to fix issues. Unfortunately, I did not document the packages since, my computer was essentially out of commission, and I was frustrated. The only package I remember from that time was ◊strong{lm-sensors} and at least one of its dependencies. All in all, that was a frustrating time, but I finally fixed it.

Once fixed, I made sure to upgrade all packages to the latest by:

◊pre-code['((class="language-bash"))]{
frederick@...$ su
Password:
root@...# apt update
<some output goes here>
root@...# apt list --upgradable
<list of upgradable packages goes here>
root@...# apt upgrade
<output of apt doing the upgrade goes here>
root@...# apt autoremove
<output of apt removing unneeded packages goes here>
}

At this point, running ◊code['((class "language-bash"))]{apt update} would indicate that the system was up to date.

Now, after upgrading Ubuntu, possibly on the same day, or a few days later, I also upgraded my ◊link['((href "https://guix.gnu.org/") (title "Link to GNU Guix website"))]{GNU Guix} package manager installed in the Ubuntu system by running:
◊pre-code['((class "language-bash"))]{
$ guix pull
<lots of messages as guix is upgraded>
$ guix pull --upgrade
<lots of output as packages installed are upgraded>
}

That was successful.

Next, I tried upgrading ◊link['((href "https://guix.gnu.org/") (title "Link to GNU Guix website"))]{GNU Guix} as the root user.

◊pre-code['((class="language-bash"))]{
frederick@...$ su
Password:
root@...# guix pull
guix pull: error: directory `/var/guix/profiles/per-user/frederick' is not owned by you
}

Now, this was a new error, I had never seen before. My thoughts went, "Oh well. Guix breaks sometimes and then gets fixed down the line, so not to worry."

This is where our story starts.

◊h2{Let's Play with Drracket}

Now, because I like learning new things, I have a bunch of compilers and interpreters installed on my computer. To try and avoid polluting my main system (because it is fragile, and I'd rather not deal with the fallout), most of these languages are installed via ◊link['((href "https://guix.gnu.org/") (title "Link to GNU Guix website"))]{GNU Guix}. Among this is ◊link['((href "https://racket-lang.org/") (title "Link to the racket language website"))]{Racket}.

I decided to play around in ◊link['((href "https://racket-lang.org/") (title "Link to the racket language website"))]{Racket} one weekend. So I ran ◊strong{DrRacket} on my terminal. What I experienced was a surprise.

◊img['((src "static/images/drracket_with_broken_text.png") (alt "Image of drracket with broken text"))]

What fresh hell is this?!?!

I did not know, or even have a clue what was happening. I tried running my ◊link['((href "https://www.gnu.org/software/emacs/") (title "Link to the GNU Emacs website"))]{emacs} with the menu activated. I noticed the same behaviour, in that the text on the menus were also garbled.

Aha! 'So something is wrong with ◊link['((href "https://guix.gnu.org/") (title "Link to GNU Guix website"))]{GNU Guix}', I thought confidently (though wrongly, as we shall see soon).

Now I know something is wrong, but I am not in the headspace to try and troubleshoot it, so I left it as was, and went on to do something else.

◊h2{Several Weeks Later}

So, I have not played with Racket for a while, and Guix should be fixed by now, right.

◊pre-code['((class "language-bash"))]{
$ guix pull
<lots of messages as guix is upgraded>
$ guix pull --upgrade
<no package is upgraded!>
}

Wait what? No package is upgraded? Oh well! I must be having all up to date packages. Woohoo! Time to fire up drracket!

◊img['((src "static/images/drracket_with_broken_text.png") (alt "Image of drracket with broken text"))]

Ahhh! What the hell!

Wait! Also, why does it say racket is version 7.6? I thought everything was up to date. I know for a fact that Racket 7.7 is out. It says so on their website.

◊pre-code['((class "language-bash"))]{
$ racket --version
Welcome to Racket v7.6.

$ guix package --search=racket
name: racket
version: 7.6
outputs: out
...
}

◊h2{Halp IRC! My Guix No Workie!!!}

I decided to look into my Guix generations

◊pre-code['((class "language-bash"))]{
$ guix package --list-generations
Generation 1	Jan 06 2020 09:41:09
  glibc-utf8-locales	2.28	out	/gnu/store/94k5w17z54w25lgp90czdqfv9m4hwzhq-glibc-utf8-locales-2.28
  emacs	26.2	out	/gnu/store/b38pn0gnj4jsrf79lg4kr80rn5kaim0q-emacs-26.2

Generation 2	Jan 07 2020 07:30:42
 + racket	7.0	out	/gnu/store/xxkh2m9nvygb537zpxp8i2lf31yq87sw-racket-7.0
.
.
.
<truncated for brevity>
}

and looking at the list of generations, I noted a point where the glibc was upgraded:

◊code{
Generation 21	Jul 04 2020 12:56:09
 + glibc-utf8-locales	2.31	out	/gnu/store/z7a6sbvqzb5zapwpznmjkq2rsxil6i67-glibc-utf8-locales-2.31
 - glibc-utf8-locales	2.29	out	/gnu/store/n79cf8bvy3k96gjk1rf18d36w40lkwlr-glibc-utf8-locales-2.29
}

Hmm, could this be it? Could it? Let us switch back to generation 20, to test it out!

◊pre-code['((class "language-bash"))]{
$ guix package --switch-generation=20
$ drracket
}


◊img['((src "static/images/drracket_with_text_and_icons_working.png") (alt "Image of drracket with text working right"))]

Aha! I'm a genius! It is glibc that's the problem. Simple fix, just install the old glibc and everything should be okay!

◊img['((src "static/images/drracket_with_broken_text.png") (alt "Image of drracket with broken text"))]

Arrghh! Oh god! Why do you hate me this much?!?!

At this point, I am frustrated, and feeling a little (a whole <expletive> lot!) stupid.

I at this point decide to ask for help on the #guix channel on the ◊link['((href "https://freenode.net/") (title "Link to the freenode site"))]{freenode IRC network}. I get a response to try the following out:

◊pre-code['((class "language-bash"))]{
$ sudo rm -rf /var/cache/fontconfig
<lots of files being removed>
$ fc-cache -rv
<the font cache being regenerated>
}

This did not fix the issue.

I was also pointed towards looking at my environment variables and investigating whether any were causing issues. That did not bear fruit either.

At this point, my frustration is so great, that I know if I continue messing with my computer, I will mess something and probably break the system. This is not a good thing to do, when you have work to do on Monday, so I left my computer, and went to watch some mind-numbing things on Netflix.

◊h2{One Week Later}

It is Saturday 18 July 2020, and I have woken up filled with zest and energy. What do we do on such a day? We mess around with something interesting. Ooooh I know! Someone on the #nairobilug channel on IRC mentioned ◊link['((href "https://beautifulracket.com/") (title "A link to a book on Racket by Matthew Butterick"))]{Beatiful Racket} and now would be an oppotune time to get started, while my moods are sky high.

◊pre-code['((class "language-bash"))]{
$ drracket
}

◊img['((src "static/images/drracket_with_broken_text.png") (alt "Image of drracket with broken text"))]

Damnit! I forgot about that!

Try upgrading guix again and upgrading packages...

◊strong{**CRICKETS**}

At this point, my frustration is frustrated!

I write up a bug report and send it to the guix bug tracker.

What to do now? What to do now? You know what, that dog over there needs a walk! I leave to take my dog out for a walk, and just calm down.

Several hours later, I am back, have cleaned and dried the dog, settled in the house, and taken some water. I decide to try and upgrade guix as root, you know, maybe it might work...

◊pre-code['((class="language-bash"))]{
frederick@...$ su
Password:
root@...# guix pull
guix pull: error: directory `/var/guix/profiles/per-user/frederick' is not owned by you
}

Wait a minute! What does this mean. I mean, I saw it last time, but ignored it.

Who am I right now?

◊pre-code['((class="language-bash"))]{
root@...# who
frederick :0           2020-07-18 14:16 (:0)
}

Wait a minute? Why am I ◊strong{frederick}. I mean, it is my name, but I have done ◊strong{su} and should be root. Could this be the issue? Do I need to brute force root? Let us try that!

I press ◊strong{Ctrl+Alt+F3} to connect to a virtual terminal on ◊strong{tty3}.

◊pre-code['((class "language-bash"))]{
Ubuntu 20.04 LTS localhost tty3

localhost login: root
Password:

Welcome to Ubuntu 18.04.3 LTS (GNU/Linux 4.15.0-1060-aws x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

  System information as of Sat Jul 18 14:26:23 UTC 2020

  System load:  0.0               Processes:           93
  Usage of /:   7.8% of 48.41GB   Users logged in:     1
  Memory usage: 29%               IP address for eth0: 172.20.3.31
  Swap usage:   0%

 * "If you've been waiting for the perfect Kubernetes dev solution for
   macOS, the wait is over. Learn how to install Microk8s on macOS."

   https://www.techrepublic.com/article/how-to-install-microk8s-on-macos/

53 packages can be updated.
0 updates are security updates.

root@localhost:~# who
root    tty3    2020-07-18 15:33
}

Okay! Now I am truly root. Wait a minute! What the heck is that? Why do we have 53 packages needing update? I swear my system was up to date a minute ago. Let's have a look.

◊pre-code['((class "language-bash"))]{
root@localhost:~# apt list --upgradable
<list of upgradable packages>
}

What? I swear I upgraded all those packages when I upgraded from Ubuntu 18.04 to 20.04, and also, in the days since. What is happening here?

It occurs to me at this point, that Ubuntu might be the culprit here. It might be I've been blaming Guix for the sins of Ubuntu.

Let us run the upgrade.

◊pre-code['((class "language-bash"))]{
root@localhost:~# apt upgrade
<upgrading all those packages that should have been upgraded>

root@localhost:~# guix pull
<lots of messages as guix is upgraded>
}

Okay! Some success. Let us go back to the graphical session. ◊strong{Alt+F1}

Now, let us see if Racket 7.7 shows up in the list of packages:

◊pre-code['((class "language-bash"))]{
$ guix package --search=racket
name: racket
version: 7.7
outputs: out
...
}

Okay! Progress. Let us upgrade just racket to see if it fixes stuff.

◊pre-code['((class "language-bash"))]{
$ guix package --upgrade=racket
<lots of output as guix does the upgrade>

$ racket --version
Welcome to Racket v7.7.
$ drracket
}

◊img['((src "static/images/drracket_with_racket77.png") (title "Working drracket with Racket 7.7") (alt "Working drracket with Racket 7.7"))]

Great success! Drinks all around!

The culprit was Ubuntu all along, and the error was hidden, for some inexplicable reason!

Now off to upgrade all the other packages installed with guix.

◊h2{Moral of the Story}

Computers are dumb!
