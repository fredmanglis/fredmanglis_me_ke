#lang pollen

◊h1{Ramblings}

◊h2{Introduction}

This file shall contain my ramblings, to help map out some of my ideas, which I can then re-organise as appropriate into various coherent (hopefully) articles.

◊h2{Punitive Versus Corrective Confinement}

We really need to move from punitive to corrective confinement, as regards our prison systems.

https://www.apa.org/monitor/julaug03/rehab



◊h2{Education}

In this country, we have a problem. I will focus on college/university-level education for now.

I think the structure of the education is blind to that fact that there are students out there, due to the vagaries of life, find themselves in a situation where they either cannot pay for their fees at all, or they can, but by, say, sacrificing some time out of their day to day life, to get a job to enable them pay.

As is, the system does not seem, as far as I can tell, to have a way for such students to continue their education.

It essentially punishes these students for having things go wrong in their lives in a way that they, the student, cannot have foreseen or done anything to change or control. This is, by many measures, a very unfair situation.

I think this goes much farther that education. How many people have lost a job due to getting sick, or any other major life-altering mishap?

These people should really not have to face this kind of debilitation, as regards their ability to improve their abilities, and/or make a living.

We need systems that work for everyone. This sentiment is in no way new. We need systems that work for you when you are young, old, rich, poor, able-bodies, disabled, etc. They should work for you if you transition from any combination of these states to another, due to mishaps, good luck, time or whatever it may be.

To start with, we need to raise the level of our discourse. Each of us, as individuals need to understand that someone else disagreeing with our opinions is not an attack on our core values. We also need to accept that enforcing the right to free speech, does not mean that you must agree, or even engage with all the opinions out there.

There is a balance to be struck. The pitfall, in striking this balance, is the slide into the "othering" of people you disagree with. This therefore calls each individual to examine their responses to various situations, and be willing to admit when they are wrong, and apologise where necessary.

We need to work together, to fix our problems, as Kenyans, and in the larger context, as human beings, worldwide. We need to come together, with all our differeing views, and accept each other. We need to take personal and collective responsibility on the oppression and subjugation of our fellow man, either directly as individuals, or via various privileges and entitlements afforded to us by various accidents, e.g. birth, connections, tribal and racial advantages, historical events, etc.

It is time we look at the fundamental rights as are enshrined in our constitution. We have failed in enforcing these rights at every turn, and have turned our constitution into nothing more than glorified tissue paper.

Let us break this down, as per [[http://www.kenyalaw.org:8181/exist/kenyalex/actview.xql?actid=Const2010][CONSTITUTION OF KENYA, 2010]]

◊h3{Fundamental Rights and freedoms that may not be limited}

◊h4{freedom from torture and cruel, inhuman or degrading treatment or punishment}

◊h3{The Right to Human Dignity}

We have failed miserably at this, both at the individual and state level. What
form of dignity is it:

- to have starving people as food goes to waste due to stupid laws
- to have people arrested and detaine


I ask of us, that we strive to understand each other's fears, hopes and dreams. We will grow as one, or fail as the weakest among us. On that, I do not need to be a prophet to be right.