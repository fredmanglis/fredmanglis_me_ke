#lang pollen

◊(define-meta publish-date "2019-06-09T12:00:00+03:00")

◊h1{Taking a Screencast With Gstreamer}

◊h2{TLDR (Too Long! Didn't Read)}

This is very basic, and does not get you far. You will quickly run into the fact that popup menus are not captured at all.

I did not test multiplexing in sound.

This was a curiosity triggered by my infrequent need to record screencasts to explain something. In a mult-screen setup, it is annoying, since Gnome tool launched with ◊em{Ctrl + Alt + Shif + R} captures all the screens.

The simplest way to deal with this is to disconnect all other screens and record the screencast on a single screen, having the window of interest in the foreground.

More work can be done on this, but it's not a thing I need every day, so if you're trully interested in this, I would recommend getting farmiliar with GStreamer.

◊h2{Set up Length of Screencast Recording}

The first thing we will do is set up the maximum length of the screencast recordings.

Let us start by reading out the current value:

◊pre-code['((class "language-bash"))]{
$ dconf read /org/gnome/settings-daemon/plugins/media-keys/max-screencast-length
uint32 30
}

As you can see, the default value is 30 seconds. Now we can set that to a different value. If we set that to zero (0), then the length of the recording is unlimited, and only stopped by pressing the key combination to stop the recording.

Now, let us set the recording length to unlimited.

◊pre-code['((class "language-bash"))]{
$ dconf write /org/gnome/settings-daemon/plugins/media-keys/max-screencast-length "uint32 0"
$ dconf read /org/gnome/settings-daemon/plugins/media-keys/max-screencast-length
uint32 0
}

We write to the editor the value "◊strong{uint32 0}" then read it back out, to be sure it was set.

Now the Gnome screencast will keep recording until you manually stop it, or run out of space, or the universe ends (whichever comes first).

◊h2{gst-launch-1.0}

◊em{gst-launch-1.0} is part of the ◊a['((href "https://gstreamer.freedesktop.org/"))]{GStreamer  project}.

For this experiment, I used ◊a['((href "https://gstreamer.freedesktop.org/documentation/tools/gst-launch.html?gi-language=c"))]{this page} as my sample, opening it in a private browser tab of Mozilla Firefox.

Let's get right to it.

I ran the following command:

◊pre-code['((class "language-bash"))]{
$ gst-launch-1.0 --eos-on-shutdown  ximagesrc show-pointer=true use-damage=false xname="gst-launch-1.0 - Mozilla Firefox (Private Browsing)" ! video/x-raw,framerate=30/1 ! videoconvert ! queue ! theoraenc ! mux. oggmux name=mux ! filesink location=screencast.ogv
}

The option ◊em{--eos-on-shutdown} is useful to ensure that the video captured is finalised properly.

◊h3{ximagesrc}

The next section of the command is the ◊em{ximagesrc}, which defines where the source of the images will be.

You can find more documentation on this GStreamer plugin by doing:

◊pre-code['((class "language-bash"))]{
$ gst-inspect-1.0 ximagesrc
}

In the command above, we set the ◊em{xname} to the title of the window we wish to capture from. In this case, it is ◊strong{"gst-launch-1.0 - Mozilla Firefox (Private Browsing)"}. With this name, GStreamer is able to find the window which you want to use as your source.

To ensure this works right, you will need to make sure the correct tab is pre-selected in Firefox.

The other option you could use is the ◊em{xid} which expects the window ID. This is a more involved option, which I have not figured out completely.

To try it out, you could do something like:

◊pre-code['((class "language-bash"))]{
$ gst-launch-1.0 --eos-on-shutdown ximagesrc show-pointer=true use-damage=false xid=$(xwininfo -tree | egrep 'Window id:' | cut -d ' ' -f4) ! video/x-raw,framerate=30/1 ! videoconvert ! queue ! theoraenc ! mux. oggmux name=mux ! filesink location=screencast.ogv
}

This introduces ◊em{xwininfo}, which, in this particular usage, will require that you use your mouse to click on the window you want to record. You could probably use ◊em{xwinfo} with the window title, but I did not test this thoroughly.

◊h3{filesink}

The ◊em{filesink} plugin is used to save the captured data into a file on your computer. In this case, the filename is provided via the ◊em{location} option and is set as "◊strong{screencast.ogv}".

You can also inspect this for more options.

◊h3{Other Plugins and Stuff}

The other plugins used in the command are ◊em{oggmux}, ◊em{videoconvert} and ◊em{theoraenc}.

◊em{oggmux} is meant to handle the sound stream. I did not test this option. Like they say in all great books, "◊i{this is left as an exercise for the reader}" 😃.

◊em{videoconvert} specifies that the raw video data passed to it on the pipeline should undergo some conversion. No options are modified for this option, so it uses the default options.

◊em{theoraenc} encodes the video using the Theora format.

◊h2{Resources}

◊ol{
◊li{
◊a['((href "https://gstreamer.freedesktop.org/documentation/tutorials/index.html"))]{GStreamer Tutorials}}}
