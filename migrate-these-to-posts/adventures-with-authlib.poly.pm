#lang pollen

◊(define-meta publish-date "2019-11-21T17:24:00+03:00")
◊(define-meta tags '("python", "flask", "authlib", "oauth2", "openid connect"))

◊h1{Authlib: Adventures Implementing OpenID Connect Server}

This blogpost was mainly obsolete as of 24 November 2019, since the merging of ◊a['((href "https://github.com/authlib/example-oidc-server/pull/1"))]{this pull request}. The most useful part will be probably the updates at the end of this article.

◊h2{TLDR (Too Long! Didn't Read)}

Recently, I have been messing around with ◊a['((href "https://pypi.org/project/Authlib/"))]{Authlib 0.13}, while trying to implement a server for the OpenID Connect flow.

There were some issues because the ◊a['((href "https://docs.authlib.org/en/latest/flask/index.html"))]{Flask OAuth Providers} documentation seems to use the newer non-depracated modules, with the ◊a['((href "https://github.com/authlib/example-oauth2-server"))]{example oauth server} (at commit 6ab7cc99eb41953a7c180afa255e1ba7fb93f7d6) and ◊a['((href "https://github.com/authlib/example-oidc-server"))]{example oidc server} (at commit 27cd151261d84fad045ee034eb51088ee032fb1e) examples using the older modules, which are in the deprecation process.

This article explores solutions to some issues encounted on the way, and presents the final result.

◊h2{Context}

To implement this, I made use of the structure provided by the examples noted in the TLDR section above, but used the code in the documentation, and templates from the example.

I essentially typed in the code in the documentation for both the ◊a['((href "https://docs.authlib.org/en/latest/flask/2/index.html"))]{Flask OAuth 2.0 Server} section, including the ◊a['((href "https://docs.authlib.org/en/latest/flask/2/openid-connect.html"))]{Flask OIDC Provider} sub section.

◊h3{The Break!}

After typing in the code and running the system, I ran into my first problem, when I tried to request a token by accessing ◊pre-code['((class "language-html"))]{http://127.0.0.1:5000/oauth/authorize?client_id=${CLIENT_ID}&scope=openid+profile&response_type=code&nonce=abc} and got the following error ◊pre-code['((class "language-bash"))]{authlib.oauth2.rfc6749.errors.InvalidRequestError: invalid_request: Missing "redirect_uri" in request}

The first thing I did was try to add ◊em{&redirect_uri=http://127.0.0.1:5002/} which was what I had entered when creating the client. The error changed slightly to ◊pre-code['((class "language-bash"))]{authlib.oauth2.rfc6749.errors.InvalidRequestError: invalid_request: Invalid "redirect_uri" in request}

What to do?

I decided to look in the database created by my code. Whoops! The redirect_uri was not saved. What gives? So I looked at the database created by the example to compare, and the first thing I noted, was that the tables saving the client information were different. The one in my code was missing some columns.

Now, at this point, I could tell that the ◊em{OAuth2ClientMixin} in ◊em{authlib.flask.oauth2.sqla} was different from the one in ◊em{authlib.integrations.sqla_oauth2}. So, the first thing I did, was try to replicate the database structure by adding the missing member variables in the Client object that I extended. That did not work. So I took a break.

The next day, I decided to dig into ◊a['((href "https://github.com/lepture/authlib"))]{the guts of authlib}. After many starts and stops, frustration, tears and hurt, (I exaggerate), I found myself looking at ◊a['((href "https://github.com/lepture/authlib/blob/master/authlib/integrations/sqla_oauth2/client_mixin.py"))]{this module}. At this point, I was enlightened. The code in that module hints that the redirect uris, scope, and other bits and pieces are saved in the metadata section.

I updated my ◊code{/add-client} endpoint accordingly, and tried again. Great success!!! I received the one-time code to use to get the token. Time to get the token:

◊pre-code['((class "language-bash"))]{
$ curl -u "${CLIENT_ID}:${CLIENT_SECRET}" -XPOST http://127.0.0.1:5000/oauth/token -F grant_type=authorization_code -F code=RSv6j745Ri0DhBSvi2RQu5JKpIVvLm8SFd5ObjOZZSijohe0

... output has been truncated ...

ValueError: Could not deserialize key data
}

Oh boy! Another error! What to do?

◊h3{On Keys}

For some reason, the simple key "secret-key" did not work for me, so I had to generate a key. To do this, I followed ◊a['((href "https://gist.github.com/ygotthilf/baa58da5c3dd1f69fae9"))]{these instructions}. Now I had an RSA key.

I then converted ◊strong{the secret key} to JWK using the ideas ◊a['((href "https://docs.authlib.org/en/latest/jose/jwk.html"))]{presented here}, and put the resulting dictionary object in the application configuration.

Now, when I tried to get the token, I was successful, and could now use the resulting token to access the protected API endpoint.

You can find the ◊a['((href "https://gitlab.com/fredmanglis/my-oauth2-serve"))]{resulting code here}.

That's it. That's the whole story. I hope you enjoyed it.

◊h2{Updates}

Here are some updates after the blog post went up.

◊h3{2019-11-26: Those ◊em{GET} Arguments!}

After trying to integrate the OIDC code into an existing project, I ran into an issue that took a long time to troubleshoot. This happened when I tried to authorise a client, by visiting an endpoint like ◊pre-code['((class "language-html"))]{http://127.0.0.1:5000/oauth/authorize?client_id=${CLIENT_ID}&scope=openid+profile&response_type=code&nonce=abc} selecting "Confirm?" and submitting the form.

The system responded with ◊pre-code['((class "language-json"))]{{"error": "invalid_grant"}}

I spent quite an amount of time digging in the guts of Authlib, to finally realise that the cause of the issue was me changing the form declaration from ◊pre-code['((class "language-html"))]{<form action="" method="POST">} to ◊pre-code['((class "language-html"))]{<form action="{{url_for('someblueprint.authorize')}}" method="POST">}.

This can be fixed by ensuring you pass on the arguments as part of the form data, or rebuilding the form action such that it contains the same argument, e.g. ◊pre-code['((class "language-html"))]{<form action="{{url_for('someblueprint.authorize', **request_args)}}" method="POST">} where ◊strong{request_args} is a dict containing the arguments in the original ◊em{GET} request.
