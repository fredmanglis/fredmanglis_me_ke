#lang pollen

◊h1{Generate Column Labels for Spreadsheets}

◊h2{TLDR}

◊h2{The Basics}

Spreadsheets label their columns something like:

◊pre-code['((class "language-plaintext"))]{
A, B, ..., Z, AA, AB, ... AZ, BA, BB, ... ZZ, AAA, AAB, ... ZZZ, AAAA, ...
}

This creates a pattern that is at first glance, very similar to how other number systems behave. To demonstrate:

*a)* Octal:
◊pre-code['((class "language-plaintext"))]{
0,1,2,3,4,5,6,7,10,11,12,13,14,15,16,17,20, ...
}

*b)* Decimal:
◊pre-code['((class "language-plaintext"))]{
0,1, ... ,10, 11, ..., 20, ..., 100, 101, ...
}

*c)* Hexadecimal:
◊pre-code['((class "language-plaintext"))]{
0,1, ... , 15, A, B ..., E, F, 10, 11, ..., FF, 100, 101, ...
}

There is one problem though, if we were to follow the above pattern, then we would be working with a base 26 system, and the pattern would be:

◊pre-code['((class "language-plaintext"))]{
A, B, ..., Z, BA, BB, ... BZ, CA, BB, ... ZZ, CAA, CAB, ... ZZZ, DAAA, ...
}

rather than:

◊pre-code['((class "language-plaintext"))]{
A, B, ..., Z, AA, AB, ... AZ, BA, BB, ... ZZ, AAA, AAB, ... ZZZ, AAAA, ...
}

What is happening?

Well, for one, the spreadsheets do not count starting at zero (0), like other number systems. One can surmise this from the fact that the row numbers begin from 1, rather than zero.

Also, if A represents zero (0), then A = 0, AA = 00 = 0, AAA = 000 = 0, etc.

What we can see, is while our first (intuitive) idea on number system seems to hint at the solution, it is not sufficient to solve our problem.

Time to break out the big guns.

◊h2{Breaking Out The Python}

Having hit a wall with formulating the problem mathematically, I decided to
brute-force the solution. To do this, I broke out the python interpreter.

Why Python? Well, I was working in Python at the time, so I used it.

I typed in the initial code, that uses number-system-esque principles.

◊pre-code['((class "language-python"))]{
def row_name_from_number(num):
   letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
   row_name_list = []
   num = num - 1
   while True:
       assert num >= 0, "num MUST be greater than zero (0)"
       if num < 26:
           row_name_list = [letters[num]] + row_name_list
           return "".join(row_name_list)
       row_name_list = [letters[(num % 26)]] + row_name_list
       num = num // 26
}

Then tested it against the following:

◊pre-code['((class "language-python"))]{
def test_function(func, input_val, expected):
     actual = func(input_val)
     assert actual == expected, "Expected '{}' but got '{}'".format(expected, actual)

test_function(row_name_from_number, 1, 'A')
test_function(row_name_from_number, 2, 'B')
test_function(row_name_from_number, 26, 'Z')
test_function(row_name_from_number, 27, 'AA')
test_function(row_name_from_number, 28, 'AB')
test_function(row_name_from_number, 701, 'ZY')
test_function(row_name_from_number, 702, 'ZZ')
test_function(row_name_from_number, 703, 'AAA')
test_function(row_name_from_number, 1024, 'AMJ')
}

As expected, the first 3 tests passed successfully. An input of 1, return 'A',
2 returned 'B', and 26 returned 'Z'.

Every other tests threw an exception.

Looking at the fourth test, we get:

◊pre-code['((class "language-python"))]{
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "/tmp/test_rows.py", line 21, in <module>
    test_function(row_name_from_number, 28, 'AB')
  File "/tmp/test_rows.py", line 15, in test_function
    assert actual == expected, "Expected '{}' but got '{}'".format(expected, actual)
AssertionError: Expected 'AA' but got 'BA'
}

and similarly for the fifth:

◊pre-code['((class "language-python"))]{
...
AssertionError: Expected 'AB' but got 'BB'
}

Looking at the  all the rest of the tests, we see that they fail for the same
reason, our first implementation was too naive for the problem at hand.

We can see that the first letter in the result for the fourth and fifth row
is one more than expected.

The results for tests 

◊h2{Formulating The Problem Mathematically}   
