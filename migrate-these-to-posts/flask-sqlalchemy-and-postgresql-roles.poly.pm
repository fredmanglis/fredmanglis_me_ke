#lang pollen

◊(define-meta publish-date "2021-03-24T15:58T+03:00")
◊(define-meta tags '("python" "flask" "postgresql" "flask-sqlalchemy" "sqlalchemy"))

◊h1{Flask, SQLAlchemy and Postgresql Roles}

My experience thus far, is that Flask-SQLAlchemy does not seem to play nicely with Postgresql roles.

The following is my tale and the partial solutions I have tested along the way.

◊h2{The PostgreSQL Roles}

Postgresql has a role-based access system, that it uses to restrict access to data in the database. This role-based access system allows the distribution of privileges among various users in the organisation in such a way that the user gets access to only the data they require. It even prevents certain users from being able to add data to, or alter data already in the database.

For more on the Postgresql database roles, see the ◊a[
    '((href "https://www.postgresql.org/docs/12/user-manag.html")
      (title "PostgreSQL Database Roles Documentation"))]{
      	     PostgreSQL database roles documentation}

For the tale, there are a number of roles, which in descending privilege levels are:

◊ul['((id "top-level-roles"))]{
	◊li{Organisation manager role (◊strong{org_manager})}
	◊li{Department head role (◊strong{dep_head})}
	◊li{General user role (◊strong{general_user})}
	◊li{Login Role (◊strong{login_role})}}

The '◊em{login_role}' is then made a member of ◊strong{ALL} actual user role: something like the following,

◊lang-block["sql" '()]{
	GRANT user_a260c85c_fc2f_468c_937c_5be2a7404a3a TO login_role}

for every ◊strong{user_*} user role in the ◊em{users} table.

Each of the users in the ◊em{users} table has a database role assigned to them of the form '◊strong{user_<user-uuid-with-underscores>}', which is then given access to ◊a['((href "#top-level-roles") (title "List of top-level roles with access to the database"))]{the group roles listed above} except for the '◊em{login_role}' role and .

◊h2{The Expected Flow}

The application by default, connects to the database as the ◊strong{login_role} user, which is the only role that can log in (at least for normal application usage). The config variable for Flask, therefore, is set up something like:

◊pre-code['((class="language-python"))]{
	SQLALCHEMY_DATABASE_URI='postgresql://login_role:7h3p4s5w0rd@db.some.host:5432/db_name'}

where '◊em{7h3p4s5w0rd}' is the database password for user '◊em{login_role}', '◊em{db.some.host}' is the database server, and '◊em{db_name}' is the name of the database in use by the application.

This means, before any user logs in, the active database role, is the '◊em{login_role}' role.

When a user (say a user with uuid ◊strong{a260c85c-fc2f-468c-937c-5be2a7404a3a}) logs in, the application should run a query

◊pre-code['((class "language-sql"))]{
	SET ROLE user_a260c85c_fc2f_468c_937c_5be2a7404a3a;}

This user role, will then have access to the database, depending on ◊a['((href "#top-level-roles") (title "List of top-level roles with access to the database"))]{which group} they are a member of.

On logging out, the application should run a query

◊lang-block["sql" '()]{SET ROLE login_role;}

that should reset the permissions to those of the default login user, who only has access to the basic tables required to enable login.

◊h2{The Problem}

The application starts off with the '◊em{login_role}' level privileges, and on login, it escalates to the privileges of the user that logged in. So far so good.

On logging out, the query to reset to the default role

◊lang-block["sql" '()]{SET ROLE login_role;}

is issued, and immediately after, a query to see the active role:

◊lang-block["python" '()]{
	app.logger.debug("CURRENT USER: {}".format(
	db.session.execute("SELECT CURRENT_USER").scalar()))}

indicates in the application logs, that the default role has been reset


◊pre-code['((class "language-log"))]{"... CURRENT USER: login_role ..."}



The problem is, it is not actually reset!

The next time a new user (especially of a higher privilege) tries to log in (even from a different browser, or computer accross the world), the login fails.

The problem, according to the logs, is that the active role, is still the last user role (e.g. '◊em{user_a260c85c_fc2f_468c_937c_5be2a7404a3a}') who may not have access to the new user's data.

The fact that this also leads to a failure across browsers and computers, means the '◊strong{SET ROLE}' quer(y/ies) might be spanning connections to the database from different browser sessions.

This implies, then, that 2 users, cannot be logged in to the same application even from separate computers in different places on the planet!

◊strong{THAT IS NOT GOOD!}

◊h2{Partial Solutions (Do not Quite Work}

◊h3{Solution 01: db.engine.dispose()}

◊lang-block["python" '()]{
@app.route("/logout")
def logout():
    logout_user()
    db.session.execute("SET ROLE login_role")
    db.session.commit()
    db.engine.dispose()
}

This solution seemed to work for a while, but then the dependency versions changed, specifically the Flask-SQLAlchemy and SQLAlchemy versions.

It also leaves the question of what happens if one user logs out, while another across the world is using the application.

◊h3{Solution 02: db.session.remove()}

◊lang-block["python" '()]{
@app.route("/logout")
def logout():
    logout_user()
    db.session.flush()
    db.session.commit()
    db.session.remove()
    db.session.execute("SET ROLE login_role")
    db.session.commit()
}

◊h3{Solution 03: Scream into the Void}

I'm frustrated at this point, so I think to myself, maybe ◊a['(
    (href "https://stackoverflow.com/questions/66669265/flask-application-with-flask-sqlalchemy-and-psycopg2-does-not-reset-database-ro")
    (title "Question that I asked on StackOverflow"))]{asking a question on StackOverflow} might help.

As of this writing (◊strong{2021-03-24T15:30+03:00}), there was no answer yet to my question.

◊h3['((id "solution-04"))]{Solution 04: @app.before_request and @app.after_request}

As I awaited for a response from StackOverflow, I read up some more on ◊strong{Flask-SQLAlchemy} and ◊strong{SQLAlchemy} sessions.

There was something in there about ◊em{scoped_session} that gave me the idea that maybe, just maybe, the issue was that the default sessions for ◊strong{Flask-SQLAlchemy} were not being reset for every request.

I thought to myself, "Why not help this along, and explicitly set up the roles at the beginning and teardown at the end of the http request?".

◊lang-block["python" '()]{
@app.before_request
def setup_session():
    if current_user and current_user.is_authenticated:
       db.session.execute("SET ROLE user_{}".format(
         current_user.uuid.replace("-", "_")))
       db.session.commit()

    db.session.execute("SET ROLE login_role")
    db.session.commit()

@app.after_request
def teardown_session(request):
    db.session.flush()
    db.session.commit()
    db.session.remove()
    db.session.execute("SET ROLE login_role")
    db.session.commit()
    return request
}

This seemed to work, initially, but after a few login/logout cycles (about 3), the issue with the role not being reset would show up.

◊h3{Solution 05: SQLAlchemy session_options}

At this point, I was thoroughly frustrated, and I decided to ◊a['(
   (href "https://github.com/pallets/flask-sqlalchemy/blob/master/tests/test_sessions.py#L21-L30")
   (title "Flask-SQLAlchemy's session tests"))]{read Flask-SQLAlchemy's code} to try and figure out how they do it.

◊lang-block["python" '()]{
from flask_sqlalchemy import SQLAlchemy

def session_scope_function():
    <function-definition>

db = SQLAlchemy(session_options={"scopefunc": session_scope_function})}

The first function definition I tried was something like:

◊lang-block["python" '()]{

def session_scope_function():
    from flask_login import current_user

    if current_user and current_user.is_authenticated:
       return user.session_id

    return "login_user"
}

This solution fails, because it leads to a ◊strong{RecursionError}.

The other definition I tried was

◊lang-block["python" '()]{
def session_scope_function():
    from flask import session
    return session.get("_user_id", "login_user")
}

which also fails, since it is called outside a request context. It also has the problem that it is using ◊strong{Flask-Login}'s internals by referencing the '◊em{_user_id}' session variable.

◊h3{Solution 04: Use @app.after_request AND sqlalchemy.text}

At successful login

◊lang-block["python" '()]{
@app.route("/login")
def login():
    ...
    user.successful_login()
    stmt = text(
            "SET ROLE user_{}".format(
                user.uuid.replace("-", "_"))).execution_options(
                    autocommit=True)
    db.session.execute(stmt)
    db.session.commit()
    ...
}

and after request

◊lang-block["python" '()]{
@app.after_request
def teardown_session(request):
    stmt = text(
            "SET ROLE {}".format(
                app.config["DB_USER"])).execution_options(
                    autocommit=True)
    db.session.execute(stmt)
    db.session.commit()
    return request
}

I have yet to test this solution thoroughly, so I don't know its full performance. It does seem to work so far.

◊h2{Sidenote}

Running the '◊strong{SET ROLE ...}' queries manually via ◊strong{psql} on the database server works as expected. This tells me the issue is in my application, or the libraries I'm using.

◊h2{Conclusion}

This article (as of ◊strong{2021-0324T15:55+03:00}) offers no actual solutions, since I have yet to find any that works, or is tested well enough to assure me.

I still need to dig deeper into the guts of both ◊strong{Flask-SQLAlchemy} and ◊strong{SQLAlchemy} to try and identify a non-hacky solution to this, that works across versions of these packages correctly.

◊h2['((id "update-20210430"))]{◊strong{UPDATE}: 2021-04-30}

One of the causes of confusion I experienced with this issue was that I was working on two different systems, each with a different authentication/authorisation flow.

One of the systems required the usual email/password login flow, while the other required the use of ◊a['((href "https://oauth.net/2/") (title "OAuth 2.0 website"))]{OAuth 2.0 protocol}.

This means that the two systems will require slightly different solutions to the same problem.

For the system with the usual email/password login/authentication flow, a modification of ◊a['((href "#solution-04"))]{Solution 04} above seems to work well. We end up with:

◊lang-block["python" '()]{
from app import db
from flask_login import current_user

@app.before_request
def before_requests():
    if current_user is None or current_user.is_anonymous:
        db.session.execute(
            "SET ROLE login_role")
        db.session.commit()
    elif current_user and current_user.is_authenticated:
        db.session.execute(
            "SET ROLE user_{}".format(current_user.uuid.replace("-", "_")))
        db.session.commit()

@app.after_request
def after_requests(response):
    db.session.flush()
    db.session.commit()
    db.session.remove()
    return response
}

For the system requiring the ◊a['((href "https://oauth.net/2/") (title "OAuth 2.0 website"))]{OAuth 2.0} authorisation flow, we end up with the authorisation code being something like:

◊lang-block["python" '()]{
from flask import current_app

def process_token(token):
    tok = Token.query.filter_by(access_token=token).first()
    if tok:
        login_user(tok.user)
        stmt = text(
            "SET ROLE user_{}".format(
                tok.user.uuid.replace("-", "_"))).execution_options(
                    autocommit=True)
        db.session.execute(stmt)
        db.session.commit()
        return {"sub": tok.access_token, "scope": tok.scope}
}

with the ◊code['((class "language-python"))]{process_token} function being used as a decorator for all functions requiring the authorisation, or in the case of ◊a['((href "https://swagger.io/specification/") (title "OpenAPI/Swagger Specification website"))]{OpenAPI}, it could be declared as the authorisation function with something like:

◊lang-block["python" '()]{
openapi: 3.0.0
components:
  securitySchemes:
    requireOauth:
      type: http
      scheme: bearer
      x-bearerInfoFunc: app.oidc.process_token
...
}

where ◊code['((class "language-python"))]{app.oidc.process_token} is simply the full "path" to the function.