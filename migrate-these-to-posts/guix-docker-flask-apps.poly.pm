#lang pollen

◊(define-meta publish-date "2019-03-17T12:30:00+03:00")
◊(define-meta categories '("guix" "python-flask" "docker"))

◊h1{Running Flask Apps With Guix and Docker}

◊h2{TLDR}

The ◊a['((href "https://www.gnu.org/software/guix/"))]{GNU Guix} package manager is a functional package manager, that enforces repeatability of build. Used together with Docker, you can start from small software bundles that only contain the binaries you need to run your application that can be rebuilt exactly the same at a different date.

This article uses a very simple flask application to demonstrate how you'd use both.

◊h2{Introduction}

I have been experimenting with using ◊a['((href "https://www.gnu.org/software/guix/"))]{GNU Guix} package manager. In this article, we'll explore how one would run a flask application inside a docker container which was built from a bundle built with Guix.

For this demonstration, we shall use ◊a['((href "https://gitlab.com/fredmanglis/hello-flask"))]{this simple flask application}.

◊h3{Preliminaries}
◊strong{Note}: To follow along, you could select guix checkout ◊em{84e58064611f987ff0abf525204d7aaee17c5ed1} to get the exact same hash values as will be indicated in this article.

◊h3{Assumptions Made}

I make the following assumptions about the reader:

◊ul{
◊li{The reader is familiar with the linux command-line interface}
◊li{The reader has installed the guix package manager on their workstation, or has access to a workstation with the guix package manager installed.}
◊li{The reader is familiar with Python and the Flask framework}}

◊h2{Building the Bundle}

The bundle shall be built using guix's ◊em{pack} command. We shall provide to the command, all the application's dependencies. In this case, we only have one dependency to point to, in this case ◊em{python-flask}.

We will also need to provide ◊em{bash} and GNU's ◊em{coreutils} so that we are able to access the running Docker container later on.

Let's jump straight into it. We create the bundle:
◊pre-code['((class "language-bash"))]{
$ guix pack --format=docker python-flask bash coreutils
}

You should see some output as guix downloads and builds the bundle. The output will look something like the following:

◊pre-code['((class "language-bash"))]{
$ guix pack --format=docker python-flask bash coreutils
substitute: updating substitutes from 'https://ci.guix.info'... 100.0%
The following derivations will be built:
   /gnu/store/r39qsijrc1qkj5nzdmdl07h3mb60fmzj-profile.drv
   /gnu/store/2004z5zjna0ikx8vl1xjcgyx72gdgixr-docker-pack.tar.gz.drv
The following profile hooks will be built:
   /gnu/store/nqp8b2i8rp3lrwzww72h4qkg932hvgd3-manual-database.drv
   /gnu/store/kaqv6a9av9kkn1mz8ns0vri3d5l1k3gj-ca-certificate-bundle.drv
   /gnu/store/p476cj7w80vp1bslp3b6z3fa1hq69g7b-fonts-dir.drv
   /gnu/store/qry6hpj0v2zsvaz72dp0yd5d4n6r8sld-info-dir.drv
building CA certificate bundle...
successfully built /gnu/store/kaqv6a9av9kkn1mz8ns0vri3d5l1k3gj-ca-certificate-bundle.drv
building fonts directory...
successfully built /gnu/store/p476cj7w80vp1bslp3b6z3fa1hq69g7b-fonts-dir.drv
building directory of Info manuals...
successfully built /gnu/store/qry6hpj0v2zsvaz72dp0yd5d4n6r8sld-info-dir.drv
building database for manual pages...
Creating manual page database...
[  9/  9] building list of man-db entries...
106 entries processed in 0.0 s
successfully built /gnu/store/nqp8b2i8rp3lrwzww72h4qkg932hvgd3-manual-database.drv
building /gnu/store/r39qsijrc1qkj5nzdmdl07h3mb60fmzj-profile.drv...
successfully built /gnu/store/r39qsijrc1qkj5nzdmdl07h3mb60fmzj-profile.drv
building /gnu/store/2004z5zjna0ikx8vl1xjcgyx72gdgixr-docker-pack.tar.gz.drv...
tar: Removing leading `/' from member names
tar: Removing leading `/' from hard link targets
successfully built /gnu/store/2004z5zjna0ikx8vl1xjcgyx72gdgixr-docker-pack.tar.gz.drv
/gnu/store/aavwzrhn54l6298y76hfsl9vn05z488m-docker-pack.tar.gz
}

You'll see from your output the name of the prepared bundle. From the output above, the name of the bundle is given in the last line of the output, and is ◊em{/gnu/store/aavwzrhn54l6298y76hfsl9vn05z488m-docker-pack.tar.gz}.

Please also take note of the line that says '/successfully built /gnu/store/r39qsijrc1qkj5nzdmdl07h3mb60fmzj-profile.drv/' since we shall need that in the docker container.

We shall use this bundle to create the docker image, with the following command:

◊pre-code['((class "language-bash"))]{
$ docker load -i /gnu/store/aavwzrhn54l6298y76hfsl9vn05z488m-docker-pack.tar.gz
}

and you will get output similar to the following:

◊pre-code['((class "language-bash"))]{
$ docker load -i /gnu/store/aavwzrhn54l6298y76hfsl9vn05z488m-docker-pack.tar.gz
3df574836d92: Loading layer [==================================================>]  273.2MB/273.2MB
Loaded image: profile:gvn9fi8r49ddks8jgi0n4p6r8b13jxdp
}

Now we can list our docker images:

◊pre-code['((class "language-bash"))]{
$ docker images
REPOSITORY          TAG                                IMAGE ID            CREATED             SIZE
profile             gvn9fi8r49ddks8jgi0n4p6r8b13jxdp   8e92337a7fc7        49 years ago        245MB
}

Now, let us find the profile where our dependencies were installed:

◊pre-code['((class "language-bash"))]{
$ docker run -it /bin/bash
docker: Error response from daemon: OCI runtime create failed: container_linux.go:344: starting container process caused "exec: \"/bin/bash\": stat /bin/bash: no such file or directory": unknown.

$ docker export $(docker ps -lq) | tar tf - | grep '\-profile/bin'
gnu/store/gvn9fi8r49ddks8jgi0n4p6r8b13jxdp-profile/bin/
gnu/store/gvn9fi8r49ddks8jgi0n4p6r8b13jxdp-profile/bin/.flask-real
gnu/store/gvn9fi8r49ddks8jgi0n4p6r8b13jxdp-profile/bin/[
gnu/store/gvn9fi8r49ddks8jgi0n4p6r8b13jxdp-profile/bin/b2sum
gnu/store/gvn9fi8r49ddks8jgi0n4p6r8b13jxdp-profile/bin/base32
gnu/store/gvn9fi8r49ddks8jgi0n4p6r8b13jxdp-profile/bin/base64
gnu/store/gvn9fi8r49ddks8jgi0n4p6r8b13jxdp-profile/bin/basename
gnu/store/gvn9fi8r49ddks8jgi0n4p6r8b13jxdp-profile/bin/bash
.
.
.
$ 
}

Now, we know where the binaries are in the docker image and therefore we can run an interactive bash session from the docker image:

◊pre-code['((class "language-bash"))]{
$ docker run -it profile:gvn9fi8r49ddks8jgi0n4p6r8b13jxdp  /gnu/store/gvn9fi8r49ddks8jgi0n4p6r8b13jxdp-profile/bin/bash
bash-4.4# 
}

As you can see, we have to include the entire path name, ◊em{/gnu/store/gvn...-profile/bin/bash} in order to run any of the binaries. This does not feel like a thing someone would want to do a lot of. Lucky for us, we can specify that we want the binaries symbolically linked to the ◊em{/bin} (or any other directory) of the image.

Another thing to note, is that python is not installed, so we will have to explicitly pass it as an argument to the ◊em{guix pack} command.

Let's do this now.

◊pre-code['((class "language-bash"))]{
$ guix pack --format=docker -S /bin=bin python-flask python bash coreutils
substitute: updating substitutes from 'https://ci.guix.info'... 100.0%
The following derivation will be built:
   /gnu/store/l92zl9bz7n4h0jd3dkhyw2b9h9a1mb7p-docker-pack.tar.gz.drv
building /gnu/store/l92zl9bz7n4h0jd3dkhyw2b9h9a1mb7p-docker-pack.tar.gz.drv...
tar: Removing leading `/' from member names
tar: Removing leading `/' from hard link targets
successfully built /gnu/store/l92zl9bz7n4h0jd3dkhyw2b9h9a1mb7p-docker-pack.tar.gz.drv
/gnu/store/f7sxcvq4mbp5b9dkk5i16zi5qxwyi364-docker-pack.tar.gz
}

Load the image:

◊pre-code['((class "language-bash"))]{
$ docker load -i /gnu/store/f7sxcvq4mbp5b9dkk5i16zi5qxwyi364-docker-pack.tar.gz
4631ff462ab6: Loading layer [==================================================>]  325.4MB/325.4MB
Loaded image: profile:902dxrvdw9n7w4j36331xc9h6bn6z7vv
}

Now we can run bash.

◊pre-code['((class "language-bash"))]{
$ docker run -it profile:902dxrvdw9n7w4j36331xc9h6bn6z7vv /bin/bash
bash-4.4#
}

and similarly, we can run Python

◊pre-code['((class "language-bash"))]{
$ docker run -it profile:902dxrvdw9n7w4j36331xc9h6bn6z7vv /bin/python3
Python 3.7.0 (default, Jan  1 1970, 00:00:01) 
[GCC 5.5.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> from flask import Flask
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
ModuleNotFoundError: No module named 'flask'
>>>
}

Fascinating, right? We did install Flask, right? Let us run this again, but via the bash installed in the docker container.

◊pre-code['((class "language-bash"))]{
$ docker run -it profile:902dxrvdw9n7w4j36331xc9h6bn6z7vv /bin/bash
bash-4.4# python3
Python 3.7.0 (default, Jan  1 1970, 00:00:01) 
[GCC 5.5.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> from flask import Flask
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
ModuleNotFoundError: No module named 'flask'
>>> quit()
bash-4.4# env  PYTHONPATH=$PYTHONPATH:/gnu/store/902dxrvdw9n7w4j36331xc9h6bn6z7vv-profile/lib/python3.7/site-packages python3
Python 3.7.0 (default, Jan  1 1970, 00:00:01) 
[GCC 5.5.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> from flask import Flask
>>>
}

Aha! So, Flask is installed, but it is not visible to Python by default. What do we do now?

We need to somehow, enable Python to find the modules.

◊h3{Load the ◊em{/gnu/store/...-profile/etc/profile} file}

The first thing we can do, is load the ◊em{etc/profile} file in the guix profile created in the bundle. The easiest way, is to make it available to bash at startup.

We can do this by modifying the command building the image slightly:

◊pre-code['((class "language-bash"))]{
$ guix pack --format=docker -S /bin=bin -S .bashrc=etc/profile python-flask python bash coreutils
}

This tells guix to create a new symbolic link at ◊em{/.bashrc} that points to the
◊em{etc/profile} file.

Now, when we load the bundle into docker and run ◊em{bash} we see that the appropriate environment variables are set up:

◊pre-code['((class "language-bash"))]{
$ docker run -it profile:902dxrvdw9n7w4j36331xc9h6bn6z7vv /bin/bash
bash-4.4# echo $PATH
/gnu/store/902dxrvdw9n7w4j36331xc9h6bn6z7vv-profile/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
bash-4.4# echo $PYTHONPATH
/gnu/store/902dxrvdw9n7w4j36331xc9h6bn6z7vv-profile/lib/python3.7/site-packages
bash-4.4# python3
Python 3.7.0 (default, Jan  1 1970, 00:00:01) 
[GCC 5.5.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> from flask import Flask
>>>
}

Wohoo! Great success!

Now let us try running ◊em{Python} directly:

◊pre-code['((class "language-bash"))]{
$ python3
Python 3.7.0 (default, Jan  1 1970, 00:00:01) 
[GCC 5.5.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> from flask import Flask
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
ModuleNotFoundError: No module named 'flask'
>>>
}

Arrgh! ◊em{Python}, run directly from the docker image, does not have access to the installed modules.

So, I need to investigate this further, and write up a follow up. For now, let us proceed to write our ◊em{Dockerfile} to use our new bundle to run our swanky, new flask application.

◊h2{Enter Docker}

Now, we will use docker to run the new application. First, let us write a ◊em{Dockerfile} to specify our bundle as the base image, and pull in our application.

We will need to add a few more binaries to our bundle, to enable us to fetch the application's code into the image and run it. Let us do that:

◊pre-code['((class "language-bash"))]{
$ guix pack --format=docker -S /bin=bin -S .bashrc=etc/profile -S /etc/ssl=etc/ssl python-flask python bash coreutils wget tar gzip nss-certs
}

We need to provide ◊em{wget} to download the files. ◊em{wget} requires ◊em{nss-certs} to verify the https certicates. We symlink ◊em{/gnu/store/..-profile/etc/ssl} to ◊em{/etc/ssl} so that ◊em{wget} can find the certificates.

We require ◊em{tar} and ◊em{gzip} to extract the downloaded archive.

◊pre-code['((class "language-bash"))]{
$ docker load -i /gnu/store/bnvcnklfvxda7khgf2cly45ab2rl9wk1-docker-pack.tar.gz
f75c076d148b: Loading layer [==================================================>]  393.4MB/393.4MB
Loaded image: profile:yag3frddn5p2crz14xqk0aahzp2wjdjm
}

We will use the image thus created in our ◊em{Dockerfile}

◊pre-code['((class "language-plaintext"))]{
FROM profile:yag3frddn5p2crz14xqk0aahzp2wjdjm
EXPOSE 5000
RUN wget https://gitlab.com/fredmanglis/hello-flask/-/archive/master/hello-flask-master.tar.gz
RUN tar xvzf hello-flask-master.tar.gz
RUN rm hello-flask-master.tar.gz
RUN cd /hello-flask-master
WORKDIR /hello-flask-master
ENV FLASK_APP=run.py FLASK_RUN_HOST=0.0.0.0 FLASK_RUN_PORT=5000
}

to build the image

◊pre-code['((class "language-bash"))]{
$ docker build -t fredmanglis/hello-flask .
}

that we can then run

◊pre-code['((class "language-bash"))]{
$ docker run -p 9999:5000 fredmanglis/hello-flask flask run
}

Now, you can try and load the page in your browser by visiting "http://127.0.0.1:9999" or with curl:

◊pre-code['((class "language-bash"))]{
$ curl -X GET "http://127.0.0.1:9999"
Hello World!
}

◊h2{Why All This?}

Why would we do all this?

Well, the first reason is that we can.

The second, more important reason, is that, there comes a need to ensure that your application's environment is thoroughly specified, to ensure that your development environment, and your production environment are as similar as possible.

With ◊a['((href "https://www.gnu.org/software/guix/"))]{GNU Guix}'s functional package management, you can ensure to a high degree of certainty, that the dependencies used are exactly the same no matter the environment.

You can also use Guix to generate a base image, that you can then use as a base container to run your application.

Normally, you'd base your image on an existing image with a full OS, like a base Ubuntu image, that you then need to run a update on to pull in any updates. This means that the image you end up running your application on might vary depending on when you build it.

Using Guix, that concern is eliminated, since you only ever have the binaries that you want, that are thoroughly specified, and whose build is repeatable.

◊h2{Further Research}

I need to figure out how to enable the python in the docker container to access the installed packages when run directly.
