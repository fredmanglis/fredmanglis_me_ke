#lang pollen

◊(define-meta publish-date "2021-02-24T08:26:00+03:00")
◊(define-meta tags '("git" "ssh-agent" "ssh-add" "docker"))

◊h1{Using SSH Keys in Docker to Authenticate with git}

Because life has no respect whatsoever for humans, you will find youself needing to do some devops stuff. In that state, you will encounter a package/application that has a dependency whose code is hosted on one of those development platforms like ◊a['((href "https://gitlab.com") (title "link to GitLab"))]{GitLab} and ◊a['((href "https://bitbucket.org") (title "link to BitBucket"))]{BitBucket} etc.

I found myself in such a situation while working on some Clojure code, and had
to use
◊a['((href "https://github.com/reifyhealth/lein-git-down")
    (title "lein-git-down repo"))]{lein-git-down} to fetch the dependencies.

It all worked beautifully on my development computer, but then, I had to go and try to deploy it. Whooo!

I spent some time searching online for solutions, and most did not work for me.

The one that worked for me was to use ssh-agent in conjunction with ssh keys.

◊h2{Using ◊strong{ssh-agent} and ssh keys}

The first step is to generate a new ssh key. I recommend to start off with a key with no passphrase.

For example, to create a key, for use with
◊a['((href "https://gitlab.com") (title "link to GitLab"))]{GitLab} you can do:

◊pre-code['((class "language-bash"))]{
$ ssh-keygen
Generating public/private rsa key pair.
Enter file in which to save the key (/home/your-username/.ssh/id_rsa): ~/.ssh/a-gitlab-key
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
...
}

This creates two files in ◊em{~/.ssh} named ◊strong{a-gitlab-key} and
◊strong{a-gitlab-key.pub}.

Register this key with
◊a['((href "https://gitlab.com") (title "link to GitLab"))]{GitLab}.

Now, to enable your docker container to access gitlab, you need to copy the private key onto the container. You can do this by adding something like:

◊pre-code['((class "language-dockerfile"))]{
RUN mkdir ~/ssh-keys
COPY /home/your-username/.ssh/a-gitlab-key ~/ssh-keys
}

to your Dockerfile.

Now, we need to start the ◊strong{ssh-agent}, add the key and fetch the dependencies. You might be tempted to do the following in your Dockerfile:

◊pre-code['((class "language-dockerfile"))]{
RUN eval $(ssh-agent -s)
RUN ssh-add ~/ssh-keys/a-gitlab-key
RUN lein deps}

and you'll quickly run into issues. The ◊em{eval} command does not create a new layer in docker, so by the time you run the other command, the environment that it should have set up does not exist any more.

You thus need to do it as follows:

pre-code['((class "language-dockerfile"))]{
RUN eval $(ssh-agent -s) && ssh-add ~/ssh-keys/a-gitlab-key && lein deps}

◊h2{References}

These references helped me out while trying to figure this stuff out:

- ◊a['((href "https://docs.gitlab.com/ee/ci/ssh_keys/")
       (title "Using SSH keys with GitLab CI/CD"))]{
       Using SSH keys with GitLab CI/CD}
- ◊a['((href "https://docs.gitlab.com/ee/user/project/new_ci_build_permissions_model.html#dependent-repositories"))]{This GitLab-specific document} might be useful in some cases.

◊h2{Conclusion}

This is a quick and dirty reference, and it does not go into detail about things such as Docker, Clojure etc.

The concepts shown here are demonstrated using GitLab, but they are not GitLab-specific. Pick the ideas and concepts and apply them wherever you need.