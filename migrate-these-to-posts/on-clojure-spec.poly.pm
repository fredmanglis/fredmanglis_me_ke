#lang pollen

◊(define-meta tags '("clojure" "spec"))

◊h1{On Clojure Spec}

I found myself recently, needing to write some tests for some existing code.

Now, somehow, I had come across the idea that Clojure's Spec library has the ability to generate tests for you, once you define the specs. I had some passing familiarity with spec, so I (smugly) thought to myself, "Hmm, I could just use spec and have it generate the tests for me, then I don't have to do anything."

Well, let me tell you Maina, I was in for a little more of an adventure than I had anticipated.

◊h2{Writing the Specs}

Writing the specs was not difficult. Once one spends a little while looking over the ◊strong{Clojure Spec Guide}◊sup{◊a['((href "#footnote-specguide"))]{1}}, they can then write simple specs, and then compose those simple specs into more complex ones.

The idea sounds (and in practice is) simple.

◊h2{Generating the Tests}

Now that one has written the specs, time to generate the tests, right?

Right.

How do we go about this...

◊h2{References}

◊ol{
  ◊li{◊a['((id "footnote-specguide"))]{
    ◊strong{Clojure Spec Guide}: ◊a['((href "https://clojure.org/guides/spec")
                                      (title "Link to Clojure Spec Guide"))]{
				        https://clojure.org/guides/spec}}}
  ◊li{◊a['((id "footnote-specapi"))]{
    ◊strong{Clojure Spec API}: ◊a['((href "https://clojure.github.io/spec.alpha/")
                                    (title "Link to Clojure Spec API"))]{
				      https://clojure.github.io/spec.alpha/}}}
  ◊li{◊a['((id "footnote-mainakingangi"))]{
    ◊strong{"Let me tell you Maina"}: A kenyan colloquialism originating from
      the "Maina and King'ang'i" radio show}}
  ◊li{◊a['((id "footnote-testcheck"))]{◊strong{clojure.test.check}: }}
  ◊li{◊a['((href "#"))]{
    https://stackoverflow.com/questions/41176696/clojure-spec-custom-generator-for-java-objects}}}
