#lang pollen

◊(define-meta publish-date #f)
◊(define-meta tags '("python" "flask" "postgres" "roles"))

◊h1{Python Flask and Multiple PostgreSQL User Roles}

◊h2{Introduction}

There comes a time when your application needs better security than th(at/ose) flask application(s) in the tutorial(s) you used to learn how to flask. Sometimes, it is not even that you wanted more security yourself, rather, your new, more competent PostgreSQL database administrator just will have none of that "Just allow the application to log in with the database administrator user, and we will do access control in the code." schtick that you've been successfully using this far.

So, what do you do? You're only a specific PostgreSQL role to log in to the database. This role only has basic access to the 'users' table in the database, to allow your application's users to log in, but nothing else.

We shall explore one possible route you could take to achieve this.

◊h3{Assumptions Made About the Reader}

I make the following assumptions about you, dear reader:

◊ul{
◊li{you are comfortable on a linux terminal}
◊li{you have some basic knowledge on ◊a['((href "https://palletsprojects.com/p/flask/"))]{Python's flask framework}}
◊li{you are comfortable with ◊a['((href "https://www.postgresql.org/"))]{PostgreSQL} and its ◊a['((href "https://www.postgresql.org/docs/9.2/app-psql.html"))]{psql client}}
◊li{you like learning new things}}

◊h2{TODOs}

- Set up the sample database
- Set up users
- Set up flask and the login
- *SET ROLE* in User model(s)
- Set up roles
- Set up RLS policies
- table privileges
- sequence privileges