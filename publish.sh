#!/bin/sh

guix shell --container --manifest=manifest.scm -- haunt build

if [ $? -eq 0 ]
then
    rsync -avz web fredmanglis.me.ke:~/public-www/
else
    echo "Could not build and publish the site!"
fi
