(define-module (fredm themes static-page)
  #:use-module (haunt site)
  #:use-module (haunt post)
  #:use-module (haunt builder blog)
  #:use-module (fredm themes blog)
  #:export (static-page-theme))

(define (static-page-layout site title body)
  `((doctype "html")
    (head
     (meta (@ (charset "utf-8")))
     (meta (@ (name "viewport")
	      (content "width=device-width, initial-scale=1.0")))
     (title ,(string-append title " -- " (site-title site)))
     (link (@ (rel "stylesheet")
	      (href "../static/css/styles.css"))))
    

    (body
     (section (a (@ (href "../index.html")) (h1 ,(site-title site))))
     (div ,body))))

(define static-page-theme
  (theme #:name "fredm"
	 #:layout static-page-layout
	 #:post-template blog-post-template
	 #:collection-template blog-collection-template))
