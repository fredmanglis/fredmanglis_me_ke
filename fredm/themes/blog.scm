(define-module (fredm themes blog)
  #:use-module (haunt site)
  #:use-module (haunt post)
  #:use-module (haunt builder blog)
  #:export (blog-theme
	    blog-post-template
	    blog-collection-template))

(define (blog-layout site title body)
  `((doctype "html")
    (head
     (meta (@ (charset "utf-8")))
     (meta (@ (name "viewport")
	      (content "width=device-width, initial-scale=1.0")))
     (title ,(string-append title " -- " (site-title site)))
     (link (@ (rel "stylesheet")
	      (href "static/css/styles.css"))))
    

    (body
     (section (a (@ (href "index.html")) (h1 ,(site-title site))))
     (div ,body))))

(define (blog-post-template post)
  `((section (h1 ,(post-ref post 'title))
	     (span "by " ,(post-ref post 'author)
		   " -- " ,(date->string* (post-date post)))
	     (div (@ (class "container"))
		  ,(post-sxml post)))))

(define (blog-collection-template site title posts prefix)
  (define (post-uri post)
    (string-append (or prefix "") "/"
		   (site-post-slug site post) ".html"))
  `((section
     (nav (@ (id "auxilliary-nav"))
	  (a (@ (href "pages/about.html")) About)
	  (a (@ (href "pages/links.html")) Links)
	  (a (@ (href "pages/where-to-buy.html")) "Where to Buy"))
     (h3 ,title)
     (ul ,@(map (lambda (post)
		  `(li
		    (a (@ (href ,(post-uri post)))
		       ,(post-ref post 'title))
		    " -- "
		    ,(date->string* (post-date post))))
		posts)))))

(define blog-theme
  (theme #:name "fredm"
	 #:layout blog-layout
	 #:post-template blog-post-template
	 #:collection-template blog-collection-template))
