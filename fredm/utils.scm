(define-module (fredm utils)
  #:export (anchor
	    import-js
	    import-css
	    source-code
	    highlight-js
	    footnote-entry))

(define (import-css href) `(link (@ (rel "stylesheet") (href ,href))))
(define (import-js src) `(script (@ (src ,src) (text "text/javascript"))))

(define (highlight-js)
  (import-js
   "https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.6.0/highlight.min.js"))

(define* (anchor text uri #:optional (title "a link"))
  `(a (@ (href ,uri) (title ,title))
      ,text))

(define* (source-code text #:key (class ""))
  `(pre
    (code (@ (class ,(string-append class " hljs")))
	  ,text)))

(define* (footnote-entry id text link #:optional (link-description ""))
  `(span (@ (id ,id) (class "footnote-entry"))
	 (string-append ,text ": ")
	 ,(anchor link link link-description)))
