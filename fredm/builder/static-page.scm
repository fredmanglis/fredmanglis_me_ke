(define-module (fredm builder static-page)
  #:use-module (haunt site)
  #:use-module (haunt post)
  #:use-module (haunt html)
  #:use-module (haunt utils)
  #:use-module (haunt reader)
  #:use-module (srfi srfi-26)
  #:use-module (haunt artifact)
  #:use-module (haunt builder blog)
  #:use-module (haunt reader skribe)
  #:export (static-page))

(define (post->page site post theme dest)
  (let ((base-name (string-append (site-post-slug site post)
				  ".html"))
        (title (post-ref post 'title))
        (body ((theme-post-template theme) post)))
    (serialized-artifact (string-append dest "/" base-name)
                         (with-layout theme site title body)
                         sxml->html)))

(define* (static-page directory theme
		      #:optional (dest directory))
  "Return a builder procedure that builds the static page(s) in DIRECTORY."
  (lambda (site posts)
    (display
     (string-append "Building files in '" directory "'"))
    (newline)
    (let ((static-pages (if (file-exists? directory)
			    (read-posts directory
					(site-file-filter site)
					(site-readers site)
					(site-default-metadata site))
			    '()))
	  (dest-dir (absolute-file-name
		     (string-append (site-build-directory site) "/" dest))))
      (when (file-exists? dest-dir)
	(delete-file-recursively dest-dir)
	(mkdir dest-dir))
      (map (cut post->page site <> theme
		(string-append dest))
	   static-pages))))
