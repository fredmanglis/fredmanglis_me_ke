(require 'ox-publish)

;; Set up the package installation directory to prevent them being installed
;; in ~/.emacs.d/elpa path
(require 'package)
(setq package-user-dir (expand-file-name ".packages"))
(setq package-archives '(("melpa" . "https://melpa.org/packages/")
			 ("elpa" . "https://elpa.gnu.org/packages/")))
;; Init the packages
(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))

;; install dependencies
(package-install 'htmlize)

(defun sitemap-entry (entry sitemap-style project)
  "Format the sitemap entry..."
  ;;(org-publish-find-date entry project)
  (let ((snippet "*... snippet goes here ...*"))
    (cond ((not (directory-name-p entry))
	   (format "*** %s\n/%s/\\\\\n%s\\\\\n[[file:%s][Read more...]]"
		   (org-publish-find-title entry project)
		   (format-time-string "%d %B %Y"
				       (org-publish-find-date entry project))
		   snippet
		   entry))
	  ((eq style 'tree)
	   ;; Return only last subdir.
	   (file-name-nondirectory (directory-file-name entry)))
	  (t entry))))

(defun sitemap (title files)
  (let ((content
	 (concat "#+TITLE: " title "\n"
		 "#+OPTIONS: toc:nil ^:{} num:nil\n"
		 "#+HTML_HEAD_EXTRA: <link rel=\"stylesheet\" href=\"static/css/styles.css\" />"
		 "\n\n"
		 "** Blog\n\n"
		 (org-list-to-generic files '(:raw t :isep "\n\n\n")))))
    (message "========================================")
    (message content)
    (message "========================================")
    content))

;; Define publishing project
(setq org-publish-project-alist
      (list
       (list"posts-and-pages"
	    :recursive t
	    :base-directory "./"
	    :base-extension "org"
	    :with-title t
	    :publishing-directory "~/Public/web/"
	    :publishing-function 'org-html-publish-to-html
	    :with-author nil
	    :with-creator t
	    :with-toc t
	    :with-date nil
	    ;;:section-numbers nil
	    :timestamp-file nil
	    :auto-preamble t
	    :with-footnotes t)
       (list "static"
	     :recursive t
	     :base-directory "./"
	     :base-extension "css\\|js\\|png\\|jpg\\|gif"
	     :publishing-directory "~/Public/web/"
	     :publishing-function 'org-publish-attachment)
       (list "sitemap"
	     :recursive t
	     :base-directory "./"
	     :base-extension "org"
	     :publishing-directory "~/Public/web/"
	     :auto-sitemap t
	     :sitemap-filename "index"
	     :sitemap-title "Frederick's Aerie"
	     ;; :sitemap-file-entry-format "%t\n%a\n%d"
	     :sitemap-style 'list
	     :sitemap-function #'sitemap
	     :sitemap-format-entry #'sitemap-entry
	     :sitemap-sort-files 'anti-chronologically)))

;; Customise the HTML output
(setq org-html-validation-link nil ;; Don't show validate link
      org-html-head-include-scripts nil ;; Get rid of default scripts
      org-html-head-include-default-style nil ;; Get rid of default styles
      org-html-htmlize-output-type 'css ;; Extract css class definitions
      org-html-head "
<link rel=\"stylesheet\" href=\"../static/css/styles.css\" />
<link rel=\"stylesheet\" href=\"../static/css/syntax-highlight.css\" />")

;; Generate the site output
(org-publish-all t)
(message "Build complete!")
